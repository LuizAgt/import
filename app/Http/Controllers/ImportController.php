<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use ZipArchive;

class ImportController extends Controller
{
    public function storeFile() {
    if (request()->file) {
        $zipFile = request()->file;

        if ($zipFile->isValid() && $zipFile->getClientOriginalExtension() === 'zip') {
            $temporaryZipPath = $zipFile->storeAs('temporary_zip', 'temporary.zip');
    
            $temporaryDirectory = storage_path('app/temporary_extracted_files');

            File::deleteDirectory($temporaryDirectory);

            $zip = new ZipArchive;
            if ($zip->open(storage_path('app/' . $temporaryZipPath)) === TRUE) {
                // Criar diretório temporário se não existir
                if (!File::isDirectory($temporaryDirectory)) {
                    File::makeDirectory($temporaryDirectory);
                }

                $zip->extractTo($temporaryDirectory);
                $zip->close();

                $subfolderNames = [];
                $subfolders = File::directories($temporaryDirectory);
                foreach ($subfolders as $subfolder) {
                    $subfolderNames[] = basename($subfolder);
                }

                return view('importFile.results', compact('subfolderNames'));
               
            } else {
                echo 'Falha ao abrir o arquivo .zip';
            }

            // Apaga o arquivo .zip temporário
            Storage::delete($temporaryZipPath);
        } else {
            echo 'Arquivo .zip inválido';
        }
    } else {
        echo 'Nenhum arquivo .zip enviado';
    }
    }

    public function analyzeFile() {
      
        $folderName = request()->folder_name;

        $products = \DB::connection('sisvoo_db')->table('products')->where('name', request()->folder_name)->get();

        if($products->count() > 0) {
            $process = \DB::connection('sisvoo_db')->table('processess')->where('product_id', $products->first()->id)->pluck('name', 'id')->toArray();
        }else {
            $process = [];
        }

        $files = Storage::allFiles('temporary_extracted_files/'.$folderName);
       
        $arrayFiles = [];
        foreach ($files as $file) {
            $arrayFiles[] = basename($file);
        }
       
        return view('importFile.analyze', compact('arrayFiles', 'folderName', 'products', 'process'));
    }

    public function update() {

        $process = \DB::connection('sisvoo_db')->table('processess')->where('id', request()->process_id)->first();
        $filePath = 'temporary_extracted_files/'.request()->folder_name.'/Processos/'.request()->process_file_name.'.pdf';

        if (Storage::exists($filePath)) {
            
            try {
                // File Upload 
                $pdf = Storage::get($filePath);

                $randomName = Str::random(10);
                $s3DestinationPath = 'pdfs/' . $randomName . '.pdf';
            
                $options = ['ACL' => 'public-read'];

                Storage::disk('s3')->put($s3DestinationPath, $pdf, $options);
                
                \DB::connection('sisvoo_db')->table('processess')->where('id', request()->process_id)->update(['pdf' => $s3DestinationPath]);
                
            }catch(\Exception $e) {
                return response()->json([
                    "message"  => "Erro ao enviar arquivo!"
                ], 422);
            }

            return response()->json([
                "message"  => "Arquivo enviado!"
            ], 201);

        }else {
            return response()->json([
                "message"  => "Arquivo não encontrado!"
            ], 422);
        }
    }
}
