<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Results</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
      .spinner {
        display: none;
      }
    </style>
  </head>
  <body data-bs-theme="dark">
    <div class="container mt-5">
      <h3>Arquivos da pasta <span class="text-success">{{ $folderName }}</span></h3>
      {{-- <a class="btn btn-primary mt-3 mb-3" href="{{ route('import.results') }}" role="button">Voltar</a> --}}
      @if($products->count() > 0)
        @if($products->count() > 1)
          <p>{{ $products->count() }} Produtos encontrados com o nome {{ $folderName }}</p>
        @else
          <p>{{ $products->count() }} Produto encontrado com o nome {{ $folderName }}</p>
        @endif
      @else
      <p class="text-danger">Nenhum produto localizao</p>
      @endif
      <p>Clique <a href="/" class="text-success">aqui</a> para importar novamente</p>
      <div class="spinner text-center m-4">
        <div class="spinner-border text-primary" role="status"></div>
      </div>
      <div class="card">
        <div class="card-body">
        @foreach ($arrayFiles as $key => $file)
          @if(substr($file, -4) != '.pdf')
            <p class="text-warning">Arquivo inválido precisa ser um pdf</p>
          @endif
            <div class="row">
              <div class="col-md-7">
                <p class="mt-4">{{ $file }}</p>
              </div>
              
              @if(count($process) > 0)
              <div class="col-md-4">
                
                <label>Processo</label>
                <select id="select_{{ substr($file, 0, -4) }}" class="form-select" aria-label="Default select example">
                  @foreach($process as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-md-1">
                <button type="button" onclick="getProcessData('{{ substr($file, 0, -4) }}')" class="btn btn-primary btn-sm mt-4">Enviar</button>
              </div>
              @else
              <div class="col-md-5">
                <p class="text-danger">Nenhum processo encontrado</p>
              </div>
              @endif
            </div>
            <hr>
        @endforeach
        </div>
    </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <script>
      function getProcessData(processFileName) {
        let processId = $('#select_'+processFileName).val();
        updateProcess(processFileName, processId);
      }

      function updateProcess(processFileName, processId) {
        let route = "{{ route('import.analyze.update') }}";
        let folderName = @json($folderName);
        $('.spinner').show();
        $.ajax({
            type: "POST", 
            dataType: "json", 
            url: route,
            data: {
            process_file_name: processFileName,
            process_id: processId,
            folder_name: folderName,
            _token: "{{ csrf_token() }}",
            },
            success: function(response) {
              if(response.message) {
                swal ( "Sucesso!" ,response.message,  "success" );
              }
              $('.spinner').hide();
            },
            error: function(fail) {
              swal ( "Oops" ,fail.responseJSON.message,  "error" );
              $('.spinner').hide();
            },
        });
      }
    </script>
  </body>
</html>