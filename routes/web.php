<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImportController;

Route::get('/', function () {
    return view('importFile.index');
});

Route::get('/test', function () {
    $products = \DB::connection('sisvoo_db')->table('products')->where('name', '4251342000-8')->get();
   
});


Route::prefix('import-file')->group(function () {
    Route::post('/',                [ImportController::class, 'storeFile'])->name('import.storeFile');
    Route::get('/results',          [ImportController::class, 'results'])->name('import.results');

    Route::prefix('analyze')->group(function () {
        Route::get('/analyze_file/{folder_name}',    [ImportController::class, 'analyzeFile'])->name('import.analyze.file');
        Route::post('/update',                       [ImportController::class, 'update'])->name('import.analyze.update');
    });
});

